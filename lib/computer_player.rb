class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark
  def initialize(name)
    @name = name
  end

  def display(board)
    @board = board
  end

  def get_move
    return winning_move if winning_move
    random_move
  end

  def winning_move
    return row_winning if row_winning
    return column_winning if column_winning
    return left_diagonal_winning if left_diagonal_winning
    return right_diagonal_winning if right_diagonal_winning
    false
  end

  def row_winning
    rows_check = @board.grid
    rows_check.each_index do |idx|
      if rows_check[idx].compact.length == 2 && rows_check[idx].compact.uniq.length == 1&& @mark == rows_check[idx].compact.uniq[0]
        return [idx, rows_check[idx].index(nil)]
      end
    end
    false
  end

  def column_winning
    columns_check = @board.grid.transpose
    columns_check.each_index do |idx|
      if columns_check[idx].compact.length == 2 && columns_check[idx].compact.uniq.length == 1 && @mark == columns_check[idx].compact.uniq[0]
        return [columns_check[idx].index(nil), idx]
      end
    end
    false
  end

  def left_diagonal_winning
    left_diag_arr = [[0,0],[1,1],[2,2]].map {|sub_arr| @board.grid[sub_arr[0]][sub_arr[1]]}
    if left_diag_arr.compact.length == 2 && left_diag_arr.compact.uniq.length == 1 && @mark == left_diag_arr.compact.unqi[0]
      return [left_diag_arr.index(nil), left_diag_arr.index(nil)]
    end
    false
  end

  def right_diagonal_winning
    right_diag_arr = [[0,2], [1,1], [2,0]].map {|sub_arr| @board.grid[sub_arr[0]][sub_arr[1]]}
    if right_diag_arr.compact.length == 2 && right_diag_arr.compact.uniq.length == 1 && @mark == right_diag_arr.compact.unqi[0]
      if right_diag_arr.index(nil) == 0
        return [0,2]
      elsif right_diag_arr.index(nil) == 1
        return [1,1]
      else
        return [2,0]
      end
    end
    false
  end


  def random_move
    open_spaces = []
    @board.grid.each_index do |idx|
      @board.grid[idx].each_index do |idx2|
        if @board.grid[idx][idx2] == nil
          open_spaces << [idx, idx2]
        end
      end
    end
    open_spaces.shuffle[-1]
  end
end
