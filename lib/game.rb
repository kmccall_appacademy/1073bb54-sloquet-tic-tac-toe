require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :player_one, :player_two, :current_player, :board

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    player_one.mark = :X
    player_two.mark = :O
    @current_player = player_one
    @board = Board.new
  end

  def play
    until @board.over?
      current_player.display(board)
      play_turn
    end
    if @board.winner
      winner = @board.winner == :X ? @player_one : @player_two
      puts "✿✿✿~#{winner.name} wins~✿✿✿"
    else
      puts "It's a tie"
    end
  end

  def play_turn
    move = current_player.get_move
    @board.place_mark(move, current_player.mark)
    switch_players!
  end

  def switch_players!
   @current_player = current_player == player_one ? player_two : player_one
 end

end
