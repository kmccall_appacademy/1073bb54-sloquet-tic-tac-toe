class Board
  attr_reader :grid
  def initialize(grid=3)
    if grid != 3
      @grid = grid
    else
      @grid = Array.new(grid){Array.new(grid)}
    end
  end

  def place_mark(position, mark)
    @grid[position[0]][position[1]] = mark
  end

  def place_marks(position_arr, mark)
    position_arr.each {|position| place_mark(position, mark)}
  end

  def empty?(position)
    @grid[position[0]][position[1]] == nil
  end

  def winner
    winning_combo_x = [:X, :X, :X]
    winning_combo_o = [:O, :O, :O]
    left_diag = [[0,0],[1,1],[2,2]]
    right_diag = [[0,2], [1,1], [2,0]]
    return :X if @grid.any? {|row| winning_combo_x == row}
    return :O if @grid.any? {|row| winning_combo_o == row}
    return :X if @grid.transpose.any? {|column| winning_combo_x == column}
    return :O if @grid.transpose.any? {|column| winning_combo_o == column}
    return :X if left_diag.map {|position| @grid[position[0]][position[1]]} == winning_combo_x
    return :O if left_diag.map {|position| @grid[position[0]][position[1]]} == winning_combo_o
    return :X if right_diag.map {|position| @grid[position[0]][position[1]]} == winning_combo_x
    return :O if right_diag.map {|position| @grid[position[0]][position[1]]} == winning_combo_o
    nil
  end

  def over?
    if !winner && @grid.any? {|sub_arr| sub_arr.include?(nil)}
      return false
    end
    true
  end
end
