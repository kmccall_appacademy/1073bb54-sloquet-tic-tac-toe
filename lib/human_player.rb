class HumanPlayer
  attr_reader :name, :mark
  def initialize(name)
    @name = name
  end

  def display(board)
    puts board.grid[0]
    puts board.grid[1]
    puts board.grid[2]
  end

  def get_move
    puts "where"
    move = gets.chomp
    position = move.split(",").map(&:to_i)
    until position.count == 2 && position.all? {|x| x.is_a? Integer}
      puts "where"
      move = gets.chomp
      position = move.split(",").map(&:to_i)
    end
    position
  end
end
